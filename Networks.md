| Network      |  Name               | Stack         | Attachable |
| -----------: | ------------------- | ------------- | :--------: |
| 10.0.1.0/24  | proxy_agent         | proxy         |            |
| 10.0.2.0/24  | intranet            | proxy         | true       |
| 10.0.3.0/24  | extranet            | proxy         | true       |
| 10.0.4.0/24  | portainer_agent     | portainer     |            |
| 10.0.5.0/24  | elk_net             | elk           |            |
| 10.0.6.0/24  | mailrelay           | mailrelay     | true       |
| 10.0.7.0/24  | monitor_net         | monitor       |            |
| 10.0.8.0/24  | monitor             | monitor       | true       |
| 10.0.9.0/24  | keycloak            | keycloak      | true       |
| 10.0.10.0/24 | system              | docker-prune  | true       |
| 10.0.11.0/24 | nexus3              | nexus3        | true       |