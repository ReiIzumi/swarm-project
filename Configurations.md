## Secrets
| Name                                           |  Stack            |
| ---------------------------------------------- | ----------------- |
| cert-wildcard-domain-cat-crt                   | proxy             |
| cert-wildcard-domain-cat-key                   | proxy             |
| cert-wildcard-domain-intranet-crt              | proxy             |
| cert-wildcard-domain-intranet-key              | proxy             |
| elk-logstash-config                            | elk               |
| elk-logstash-docker-pipeline                   | elk               |
| elk-logstash-linux-pipeline                    | elk               |
| elk-kibana-config                              | elk               |
| mailrelay-config                               | mailrelay         |
| postgres-admin-user                            | monitor           |
| postgres-admin-password                        | monitor           |
| grafana-admin-user                             | monitor           |
| grafana-admin-password                         | monitor           |
| keycloak-user                                  | keycloak          |
| keycloak-password                              | keycloak          |
| keycloak-psql-user                             | keycloak          |
| keycloak-psql-password                         | keycloak          |

## Configs
| Name                                           |  Stack            |
| ---------------------------------------------- | ----------------- |
| traefik-config-intranet                        | proxy             |
| traefik-config-extranet                        | proxy             |
| elk-elasticsearch-config                       | elk               |
| elk-logstash-pipelines                         | elk               |
| monitor-prometheus-config                      | monitor           |

## Volumes
| Path                                           | Stack             | Type      |
| ---------------------------------------------- | ----------------- | --------- |
| /mnt/portainer                                 | portainer         | GlusterFS |
| /mnt/elk                                       | elk               | GlusterFS |
| /mnt/mailrelay                                 | mailrelay         | GlusterFS |
| /mnt/monitor                                   | monitor           | GlusterFS |
| /mnt/nexus3                                    | nexus3            | NFS       |
| /mnt/minio                                     | minio             | NFS       |

## URL
| URL                                            | Proxy    | Stack             |
| ---------------------------------------------- | -------- | ----------------- |
| https://traefik.domain.intranet                | Intranet | proxy             |
| https://traefik-extranet.domain.intranet       | Extranet | proxy             |
| https://portainer.domain.intranet              | Intranet | portainer         |
| https://kibana.domain.intranet                 | Intranet | elk               |
| https://prometheus.domain.intranet             | Intranet | monitor           |
| https://grafana.domain.intranet                | Intranet | monitor           |
| https://keycloak.domain.intranet               | Intranet | keycloak          |
| https://keycloak.domain.cat                    | Extranet | keycloak          |
| https://nexus.domain.intranet                  | Intranet | nexus3            |
| https://docker-registry.domain.intranet        | Intranet | nexus3            |
| https://docker-public.domain.intranet          | Intranet | nexus3            |
| https://nexus.domain.cat                       | Intranet | nexus3            |
| https://registry.domain.cat                    | Extranet | nexus3            |
| https://minio.domain.cat                       | Intranet | minio             |

## Labels
| Key      | Value    | Description                                                           | Nodes    |
| -------- | -------- | --------------------------------------------------------------------- | -------- |
| intranet | true     | Publishes internal webs. Port 80/443                                  | Swarm1, Swarm2 |
| extranet | true     | Publishes to Internet (require open ports on the router). Port 80/443 | Swarm3 |

## Ports
| Port     | Type | Stack             | Description       |
| -------- | :--: | ----------------- | ----------------- |
| 80       | TCP  | proxy             | HTTP redirect     |
| 443      | TCP  | proxy             | HTTPS             | 
| 9100     | TCP  | -                 | Node exporter     |
| 12201    | UDP  | elk               | Docker logs       |
| 12202    | UDP  | elk               | Linux logs        |
